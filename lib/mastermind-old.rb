class Code
  attr_reader

  def initialize (pegs)
    @pegs = pegs
  end

  def pegs
    @pegs
  end

  PEGS = {
         "B" => :blue,
         "G" => :green,
         "O" => :orange,
         "P" => :purple,
         "R" => :red,
         "Y" => :yellow
       }
  def [](idx)
    pegs[idx]
  end

  def self.random
    random_pegs = []
    4.times do
      random_pegs << PEGS.values.sample(5)
    end
    random_code = Code.new(random_pegs)
  end

  def self.parse(input)
    result = []
    input.each_char do |peg|
      raise_error unless ("BGOPRY").include?(peg.upcase)
      result << PEGS[peg.upcase]
    end
    new_code = Code.new(result)
  end

  def raise_error
    raise "error!"
  end

  def exact_matches(guess)
    exact_match = 0
    (0..3).each do |idx|
      exact_match +=1 if guess[idx] == pegs[idx]
    end
    exact_match
  end

  def near_matches(guess)
    near_match = 0
    guess_count = Hash.new(0)
    code_count = Hash.new(0)
    (0..3).each do |idx|
      guess_count[guess[idx]] += 1
      code_count[pegs[idx]] += 1
    end

    (0..3).each do |idx|
      unless guess[idx] == pegs[idx]
        if pegs.include?(guess[idx]) && code_count[guess[idx]] > 0
          near_match +=1
          code_count[guess[idx]] -= 1
        end
      end
    end

    near_match
  end

  def ==(code)
    return false unless code.is_a?(Code)
    self.pegs == code.pegs

  end
end

class Game
  attr_reader :secret_code, :exact_match, :near_match

  def initialize(secret_code= Code.random)
    @secret_code = secret_code
    @num_turns = 0
  end

  #private

  def get_guess
    begin
    puts "What is your guess? (in the form RBBY or GGYP, etc.)"
    guess = gets.chomp
    Code.parse(guess)
    rescue
      puts "There was an error!  Try again"
      
    end
  end

  def display_matches(guess)
    exact_matches = @secret_code.exact_matches(guess)
    near_matches = @secret_code.near_matches(guess)
    puts "Your guess has #{exact_matches} exact matches and #{near_matches} near matches!"
  end
end
